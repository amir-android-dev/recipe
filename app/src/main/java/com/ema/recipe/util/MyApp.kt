package com.ema.recipe.util

import dagger.hilt.android.HiltAndroidApp


import android.app.Application
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump

@HiltAndroidApp
class MyApp:Application() {
    override fun onCreate() {
        super.onCreate()
        //calligraphy
        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("font/atlas_regular.ttf")

                            .build()
                    )
                )
                .build()
        )

    }
}