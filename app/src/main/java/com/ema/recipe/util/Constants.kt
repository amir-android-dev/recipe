package com.ema.recipe.util

object Constants {
    const val BASE_URL = "https://api.spoonacular.com/"
    const val MY_API_KEY = "d8556ac21d21437f864e47aa6fa7b86b"

    //api
    const val API_KEY = "apiKey"

    /***recipe key***/
    //popular
    const val SORT = "sort"
    const val NUMBER = "number"
    const val ADD_RECIPEIN_FORMATION = "addRecipeInformation"

    //recent
    const val TYPE = "type"
    const val DIET = "diet"


    //recipe value
    const val POPULARITY = "popularity"
    const val TRUE = "true"
    const val MAIN_COURSE = "main course"
    const val GLUTEN_FREE = "Gluten Free"

    //popular adapter
    const val OLD_IMAGE_SIZE = "312x231.jpg"
    const val NEW_IMAGE_SIZE = "636x393.jpg"

    //register dataStore
    const val REGISTER_USER_INFO = ("register_user_info")
    const val REGISTER_USER_NAME = ("register_username")
    const val REGISTER_HASH = ("register_hash")

    //database
    const val RECIPE_DATA_BASE = "recipe_data_base"
    const val RECIPE_TABLE = "recipe_table"

    //menu dataStore
    const val MENU_MAIN_KEY = ("menu_main_key")
    const val MENU_MEAL_ID = ("menu_meal_id")
    const val MENU_MEAL_ITEM = ("menu_meal_item")
    const val MENU_DIET_ID = ("menu_diet_id")
    const val MENU_DIET_ITEM = ("menu_diet_item")


}
