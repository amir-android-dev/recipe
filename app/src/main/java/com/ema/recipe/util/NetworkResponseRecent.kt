package com.ema.recipe.util


import retrofit2.Response

open class NetworkResponseRecent<T>(private val response: Response<T>) {

    fun generalNetworkResponse(): NetworkRequest<T> {
        return when {
            response.message().contains("timeout") -> NetworkRequest.Error("Timeout")
            response.code() == 401 -> NetworkRequest.Error("You are not authorized")
            response.code() == 402 -> NetworkRequest.Error("Your free access is expired")
            response.code() == 422 -> NetworkRequest.Error("API KEY not found")
            response.code() == 500 -> NetworkRequest.Error("Pls try later!")
            response.body().toString().isNullOrEmpty()  -> NetworkRequest.Error("No recipes is found!")
            response.isSuccessful -> NetworkRequest.Success(response.body()!!)
            else -> NetworkRequest.Error(response.message())

        }
    }
}