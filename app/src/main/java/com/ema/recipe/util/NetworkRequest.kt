package com.ema.recipe.util

sealed class NetworkRequest<T>(val data: T? = null, val massage: String? = null) {

    class Loading<T> : NetworkRequest<T>()
    class Success<T>(data: T) : NetworkRequest<T>(data)
    class Error<T>(massage: String, data: T? = null) : NetworkRequest<T>(data, massage)
}