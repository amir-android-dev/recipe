package com.ema.recipe.ui.menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.asLiveData
import androidx.navigation.fragment.findNavController
import com.ema.recipe.R
import com.ema.recipe.databinding.FragmentMenuBinding
import com.ema.recipe.util.onceObserve
import com.ema.recipe.viewmodel.MenuViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipDrawable
import com.google.android.material.chip.ChipGroup
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MenuFragment : BottomSheetDialogFragment() {

    //BINDING
    private var _binding: FragmentMenuBinding? = null
    private val binding get() = _binding!!

    //  private lateinit var viewModel: MenuViewModel
    private val viewModel: MenuViewModel by viewModels()
    private var chipCounter = 1
    private var chipMealTitle = ""
    private var chipMealId = 0
    private var chipDietTitle = ""
    private var chipDietId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // viewModel = ViewModelProvider(requireActivity())[MenuViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMenuBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            setupChip(mealChipGroup, viewModel.mealsList())
            setupChip(dietChipGroup, viewModel.dietList())
            viewModel.readStoredMenuItems.asLiveData().onceObserve(viewLifecycleOwner) {
                chipMealTitle = it.mealItem
                chipDietTitle = it.dietItem
                updatedChip(it.mealId, mealChipGroup)
                updatedChip(it.dietId, dietChipGroup)

            }
            //checking chips
            mealChipGroup.setOnCheckedStateChangeListener { group, checkedIds ->
                var chip: Chip
                checkedIds.forEach {
                    chip = group.findViewById(it)
                    chipMealTitle = chip.text.toString().lowercase()
                    chipMealId = it
                }
            }
            dietChipGroup.setOnCheckedStateChangeListener { group, checkedIds ->
                var chip: Chip
                checkedIds.forEach {
                    chip = group.findViewById(it)
                    chipDietTitle = chip.text.toString().lowercase()
                    chipDietId = it
                }
            }
            //submit
            submitBtn.setOnClickListener {
                viewModel.storeMenuItem(chipMealTitle, chipMealId, chipDietTitle, chipDietId)
                val action = MenuFragmentDirections.actionMenuFragmentToRecepieFragment()
                findNavController().navigate(action.setIsUpdated(true))
            }
        }
    }

    private fun setupChip(chipGroup: ChipGroup, list: MutableList<String>) {
        list.forEach {
            val chip = Chip(requireContext())
            val drawable =
                ChipDrawable.createFromAttributes(requireContext(), null, 0, R.style.DarkChip)
            chip.setChipDrawable(drawable)
            chip.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
            chip.id = chipCounter++
            chip.text = it
            chipGroup.addView(chip)
        }
    }

    private fun updatedChip(id: Int, view: ChipGroup) {
        if (id != 0) {
            view.findViewById<Chip>(id).isChecked = true
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}