package com.ema.recipe.ui.splash

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.asLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import coil.load
import com.ema.recipe.BuildConfig
import com.ema.recipe.R
import com.ema.recipe.databinding.FragmentSplashBinding
import com.ema.recipe.viewmodel.RegisterViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SplashFragment : Fragment() {
    //binding
    private var _binding: FragmentSplashBinding? = null
    private val binding: FragmentSplashBinding
        get() = _binding!!

    //other
    private val viewModel: RegisterViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSplashBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            //dynamic background
            bgImg.load(R.drawable.bg_splash)
            //application version
            versionTv.text = "${getString(R.string.version)}: ${BuildConfig.VERSION_NAME}"
            //auto navigate
            lifecycleScope.launch {
                delay(2000)
                //check user info
                viewModel.readData.asLiveData().observe(viewLifecycleOwner) {
                    //closing the pop back
                    findNavController().popBackStack(R.id.splashFragment, true)
                    if (it.username.isNotEmpty()) {
                        //go to main page
                        findNavController().navigate(R.id.action_to_recepieFragment)
                    } else {
                        //go to register
                        val action =
                            SplashFragmentDirections.actionToRegisterFragment()
                        findNavController().navigate(action)
                    }
                }
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}