package com.ema.recipe.ui.recepie

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import coil.load
import coil.request.CachePolicy
import com.ema.recipe.R
import com.ema.recipe.data.model.recipe.ResponseRecipe
import com.ema.recipe.databinding.ItemRecipesBinding
import com.ema.recipe.util.BaseDiffUtils
import com.ema.recipe.util.Constants.NEW_IMAGE_SIZE
import com.ema.recipe.util.Constants.OLD_IMAGE_SIZE
import com.ema.recipe.util.minToHour
import com.ema.recipe.util.setupDynamicallyColor
import javax.inject.Inject

class RecentAdapter @Inject constructor() : Adapter<RecentAdapter.MViewHolder>() {
    private lateinit var binding: ItemRecipesBinding
    private var items = emptyList<ResponseRecipe.Result>()
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MViewHolder {
        binding = ItemRecipesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        context = parent.context
        return MViewHolder()
    }

    override fun onBindViewHolder(holder: MViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun onViewAttachedToWindow(holder: MViewHolder) {
        super.onViewAttachedToWindow(holder)
        holder.initAnimation()
    }

    override fun onViewDetachedFromWindow(holder: MViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.clearAnimation()
    }

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int) = position
    override fun getItemId(position: Int) = position.toLong()

    inner class MViewHolder : ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(item: ResponseRecipe.Result) {
            val convertSummeryFromHtmlToCompat =
                HtmlCompat.fromHtml(item.summary!!, HtmlCompat.FROM_HTML_MODE_COMPACT)
            binding.recipeDescTxt.text = convertSummeryFromHtmlToCompat
            binding.recipeNameTxt.text = item.title
            binding.recipeLikeTxt.text = item.aggregateLikes!!.minToHour()
            binding.recipeTimeTxt.text = item.readyInMinutes!!.minToHour()
            binding.recipeHealthTxt.text = item.healthScore.toString()
            //image
            val imageSplit = item.image!!.split("-")
            val imageSize = imageSplit[1].replace(OLD_IMAGE_SIZE, NEW_IMAGE_SIZE)
            binding.recipeImg.load("${imageSplit[0]}-$imageSize") {
                crossfade(true)
                crossfade(500)
                memoryCachePolicy(CachePolicy.ENABLED)
                error(R.drawable.ic_placeholder)
            }
            //vegan
            if (item.vegan!!) {
                binding.recipeVeganTxt.setupDynamicallyColor(R.color.caribbean_green)
            } else {
                binding.recipeVeganTxt.setupDynamicallyColor(R.color.gray)
            }
            //healthy
            when (item.healthScore) {
                in 90..100 -> binding.recipeHealthTxt.setupDynamicallyColor(R.color.caribbean_green)
                in 60..89 -> binding.recipeHealthTxt.setupDynamicallyColor(R.color.mikado_yellow)
                in 0..59 -> binding.recipeHealthTxt.setupDynamicallyColor(R.color.tart_orange)
            }
            binding.root.setOnClickListener {
                onItemClickListener?.let { it(item) }
            }
        }

        fun initAnimation() {
            binding.root.animation = AnimationUtils.loadAnimation(context, R.anim.item_anim)
        }

        fun clearAnimation() {
            binding.root.clearAnimation()
        }
    }

    private var onItemClickListener: ((ResponseRecipe.Result) -> Unit)? = null
    fun setOnClickListener(listener: (ResponseRecipe.Result) -> Unit) {
        onItemClickListener = listener
    }

    fun updateData(data: List<ResponseRecipe.Result>) {
        val diffUtils = BaseDiffUtils(items, data)
        val diffCallBack = DiffUtil.calculateDiff(diffUtils)
        items = data
        diffCallBack.dispatchUpdatesTo(this)
    }

}