package com.ema.recipe.ui.recepie

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import coil.load
import coil.request.CachePolicy
import com.ema.recipe.R
import com.ema.recipe.data.model.recipe.ResponseRecipe
import com.ema.recipe.databinding.ItemPopularBinding
import com.ema.recipe.util.BaseDiffUtils
import com.ema.recipe.util.Constants.NEW_IMAGE_SIZE
import com.ema.recipe.util.Constants.OLD_IMAGE_SIZE
import javax.inject.Inject

class PopularAdapter @Inject constructor() : Adapter<PopularAdapter.MViewHolder>() {
    private lateinit var binding: ItemPopularBinding
    private var items = emptyList<ResponseRecipe.Result>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MViewHolder {
        binding = ItemPopularBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MViewHolder()
    }

    override fun onBindViewHolder(holder: MViewHolder, position: Int) {
        holder.bind(items[position])
    }
    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int) = position
    override fun getItemId(position: Int) = position.toLong()

    inner class MViewHolder : ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(item: ResponseRecipe.Result) {
            binding.popularNameTxt.text = item.title
            binding.popularPriceTxt.text = "${item.pricePerServing.toString()} $"
            //image
            val imageSplit = item.image!!.split("-")
            val imageSize = imageSplit[1].replace(OLD_IMAGE_SIZE, NEW_IMAGE_SIZE)
            binding.popularImg.load("${imageSplit[0]}-$imageSize") {
                crossfade(true)
                crossfade(500)
                memoryCachePolicy(CachePolicy.ENABLED)
                error(R.drawable.ic_placeholder)
            }
            binding.root.setOnClickListener {
                onItemClickListener?.let { it(item) }
            }
        }
    }

    private var onItemClickListener: ((ResponseRecipe.Result) -> Unit)? = null
    fun setOnClickListener(listener: (ResponseRecipe.Result) -> Unit) {
        onItemClickListener = listener
    }

    fun updateData(data: List<ResponseRecipe.Result>) {
        val diffUtils = BaseDiffUtils(items, data)
        val diffCallBack = DiffUtil.calculateDiff(diffUtils)
        items = data
        diffCallBack.dispatchUpdatesTo(this)
    }

}