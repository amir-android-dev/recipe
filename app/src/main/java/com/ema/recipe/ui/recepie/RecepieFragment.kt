package com.ema.recipe.ui.recepie

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewmodel.viewModelFactory
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.ema.recipe.R
import com.ema.recipe.data.model.recipe.ResponseRecipe
import com.ema.recipe.databinding.FragmentRecepieBinding
import com.ema.recipe.util.NetworkRequest
import com.ema.recipe.util.displaySnackBar
import com.ema.recipe.util.onceObserve
import com.ema.recipe.util.setupRecyclerView
import com.ema.recipe.viewmodel.RecipeViewModel
import com.ema.recipe.viewmodel.RegisterViewModel
import com.todkars.shimmer.ShimmerRecyclerView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class RecepieFragment : Fragment() {

    //BINDING
    private var _binding: FragmentRecepieBinding? = null
    private val binding get() = _binding!!

    //other
    private val registerViewModel: RegisterViewModel by viewModels()
    private val viewModel: RecipeViewModel by viewModels()

    @Inject
    lateinit var popularAdapter: PopularAdapter

    @Inject
    lateinit var recentAdapter: RecentAdapter
    private var autoScrollIndex = 0
    private val args: RecepieFragmentArgs by navArgs()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRecepieBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //show username
        lifecycleScope.launch {
            displayUserName()
        }
        //api call
        callPopularData()
        callRecentData()
        //load data
        loadPopularData()
        loadRecentData()
    }

    /***popular***/
    private fun fillPopularAdapter(results: MutableList<ResponseRecipe.Result>) {
        popularAdapter.updateData(results)
        popularAutoScroll(results)
    }

    private fun callPopularData() {
        initPopularRecycler()
        viewModel.readPopularFromDb.onceObserve(viewLifecycleOwner) { database ->
            if (database.isNotEmpty()) {
                database[0].response.results?.let { results ->
                    setupLoading(false, binding.popularList)
                    fillPopularAdapter(results.toMutableList())
                }
            } else {
                viewModel.callPopularApi()
            }
        }
    }

    private fun loadPopularData() {
        viewModel.popularRecipes.observe(viewLifecycleOwner) { response ->
            when (response) {
                is NetworkRequest.Error -> {
                    setupLoading(false, binding.popularList)
                    requireView().displaySnackBar(response.massage.toString())
                }
                is NetworkRequest.Loading -> {
                    setupLoading(true, binding.popularList)
                }
                is NetworkRequest.Success -> {
                    setupLoading(false, binding.popularList)
                    response.data?.let { data ->
                        if (data.results!!.isNotEmpty()) {
                            fillPopularAdapter(data.results.toMutableList())
                        }
                    }
                }
            }
        }
    }

    private fun initPopularRecycler() {
        val snapHelper = LinearSnapHelper()

        binding.popularList.setupRecyclerView(
            LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.HORIZONTAL,
                false
            ),
            popularAdapter
        )
        //snapHelper
        snapHelper.attachToRecyclerView(binding.popularList)
        //click
        popularAdapter.setOnClickListener {
            //todo go to detail
        }
    }

    private fun popularAutoScroll(list: List<ResponseRecipe.Result>) {
        lifecycleScope.launch {
            repeat(100) {
                delay(2000)
                if (autoScrollIndex < list.size) {
                    autoScrollIndex += 1
                } else {
                    autoScrollIndex = 0
                }
                binding.popularList.smoothScrollToPosition(autoScrollIndex)
            }
        }
    }

    /***recent***/
    private fun loadRecentData() {
        viewModel.recentRecipes.observe(viewLifecycleOwner) { response ->
            when (response) {
                is NetworkRequest.Error -> {
                    setupLoading(false, binding.recipesList)
                    requireView().displaySnackBar(response.massage.toString())
                }
                is NetworkRequest.Loading -> {
                    setupLoading(true, binding.recipesList)
                }
                is NetworkRequest.Success -> {
                    setupLoading(false, binding.popularList)
                    response.data?.let { data ->
                        if (data.results!!.isNotEmpty()) {
                            recentAdapter.updateData(data.results.toMutableList())

                        }
                    }
                }
            }
        }
    }

    private fun callRecentData() {
        initRecentRecycler()
        viewModel.readRecentFromDb.observe(viewLifecycleOwner) { database ->
            if (database.isNotEmpty() && database.size > 1 && ! args.isUpdated) {
                database[1].response.results?.let { results ->
                    recentAdapter.updateData(results.toMutableList())
                    setupLoading(false, binding.recipesList)
                }
            } else {
                viewModel.callRecentApi()
            }
        }
    }

    private fun initRecentRecycler() {
        binding.recipesList.setupRecyclerView(
            LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            ),
            recentAdapter
        )
        //click
        recentAdapter.setOnClickListener {
            //todo go to detail
        }
    }

    private fun setupLoading(isShownLoading: Boolean, shimmer: ShimmerRecyclerView) {
        shimmer.apply {
            if (isShownLoading) showShimmer() else hideShimmer()
        }
    }

    @SuppressLint("SetTextI18n")
    private suspend fun displayUserName() {
        registerViewModel.readData.collect {
            binding.usernameTxt.text =
                "${getString(R.string.hello)}, ${it.username} ${getEmojiByUnicode()}"
        }
    }

    private fun getEmojiByUnicode(): String {
        return String(Character.toChars(0x1f44b))
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}