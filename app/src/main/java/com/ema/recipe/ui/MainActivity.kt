package com.ema.recipe.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.ema.recipe.R
import com.ema.recipe.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint
import io.github.inflationx.viewpump.ViewPumpContextWrapper

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    //BINDING
    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!

    //other
    private lateinit var navHost: NavHostFragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        navHost =
            supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
        //setting up the nav background color
        binding.mainBottomNav.background = null
        //setting up the nav Bottom
        binding.mainBottomNav.setupWithNavController(navHost.navController)
        //gone menu by splash & fragment
        navHost.navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.splashFragment -> menuVisibility(false)
                R.id.registerFragment -> menuVisibility(false)
                else -> menuVisibility(true)
            }
        }
        binding.mainFabMenu.setOnClickListener {
            navHost.navController.navigate(R.id.action_to_menuFragment)
        }
    }

    private fun menuVisibility(isVisible: Boolean) {
        binding.apply {
            if (isVisible) {
                mainBottomAppBar.isVisible = true
                mainFabMenu.isVisible = true
            } else {
                mainBottomAppBar.isVisible = false
                mainFabMenu.isVisible = false
            }
        }
    }

    override fun navigateUpTo(upIntent: Intent?): Boolean {
        return navHost.navController.navigateUp() || super.navigateUpTo(upIntent)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }
}