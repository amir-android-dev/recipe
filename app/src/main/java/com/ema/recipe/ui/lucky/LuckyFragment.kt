package com.ema.recipe.ui.lucky

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ema.recipe.R
import com.ema.recipe.databinding.FragmentLuckyBinding


class LuckyFragment : Fragment() {
    //BINDING
    private var _binding: FragmentLuckyBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLuckyBinding.inflate(layoutInflater,container,false)
        return binding.root
    }


}