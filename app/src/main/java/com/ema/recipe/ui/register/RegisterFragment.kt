package com.ema.recipe.ui.register

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import coil.load
import com.ema.recipe.R
import com.ema.recipe.data.model.register.BodyRegister
import com.ema.recipe.databinding.ActivityMainBinding
import com.ema.recipe.databinding.FragmentRegisterBinding
import com.ema.recipe.util.Constants.API_KEY
import com.ema.recipe.util.Constants.MY_API_KEY
import com.ema.recipe.util.NetworkChecker
import com.ema.recipe.util.NetworkRequest
import com.ema.recipe.util.displaySnackBar
import com.ema.recipe.viewmodel.RegisterViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class RegisterFragment : Fragment() {

    //BINDING
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    //other
    private val viewModel: RegisterViewModel by viewModels()

    @Inject
    lateinit var bodyRegister: BodyRegister

    @Inject
    lateinit var networkChecker: NetworkChecker
    private var email = ""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentRegisterBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            //loading cover image
            coverImg.load(R.drawable.register_logo)
            //email
            emailEdt.addTextChangedListener {
                if (it.toString().contains("@") && it.toString().contains(".")) {
                    email = it.toString()
                    emailTxtLay.error = ""
                } else {
                    emailTxtLay.error = getString(R.string.emailNotValid)
                }
            }
            //submit register
            submitBtn.setOnClickListener {
                val firstName = nameEdt.text.toString()
                val lastName = lastNameEdt.text.toString()
                val userName = usernameEdt.text.toString()
                bodyRegister.firstName = firstName
                bodyRegister.lastName = lastName
                bodyRegister.username = userName
                bodyRegister.email = email

                lifecycleScope.launch {
                    //check network
                    networkChecker.checkNetworkAvailibility().collect { isConnected ->
                        if (isConnected) {
                            //register
                            viewModel.callRegisterApi(MY_API_KEY, bodyRegister)
                        } else {
                            requireView().displaySnackBar(
                                getString(R.string.check_your_connection)
                            )
                        }
                    }
                }
            }
            loadRegisterResponse()
        }
    }

    private fun loadRegisterResponse() {
        viewModel.registerData.observe(viewLifecycleOwner) { response ->
            when (response) {
                is NetworkRequest.Error -> {
                    requireView().displaySnackBar(response.massage.toString())
                }
                is NetworkRequest.Loading -> {}
                is NetworkRequest.Success -> {
                    response.data?.let { data ->
                        viewModel.storeData(data.username.toString(), data.hash.toString())
                        //closing the pop back
                        findNavController().popBackStack(R.id.registerFragment,true)
                        //navigate to recipe
                        findNavController().navigate(R.id.action_to_recepieFragment)
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}