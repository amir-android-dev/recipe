package com.ema.recipe.di

import android.content.Context
import androidx.room.Room
import com.ema.recipe.data.database.DatabaseRecipe
import com.ema.recipe.util.Constants.RECIPE_DATA_BASE
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Provides
    @Singleton
    fun provideDao(database: DatabaseRecipe) = database.dao()

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, DatabaseRecipe::class.java, RECIPE_DATA_BASE)
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
}