package com.ema.recipe.di


import android.content.Context
import androidx.datastore.core.DataStore

import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import com.ema.recipe.util.Constants.MENU_MAIN_KEY
import com.ema.recipe.util.Constants.REGISTER_USER_INFO
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataStoreMenuModule {

    private val Context.dataStore by preferencesDataStore(name = MENU_MAIN_KEY)

    @Provides
    @Singleton
    @Named(MENU_MAIN_KEY)
    fun provideMenuDataStore(@ApplicationContext context: Context): DataStore<Preferences> {
        return context.dataStore
    }
}