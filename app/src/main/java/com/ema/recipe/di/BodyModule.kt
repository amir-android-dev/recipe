package com.ema.recipe.di

import com.ema.recipe.data.model.register.BodyRegister
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import javax.inject.Singleton

@Module
@InstallIn(FragmentComponent::class)
object BodyModule {

    @Provides
    fun provideRegisterBody() = BodyRegister()
}