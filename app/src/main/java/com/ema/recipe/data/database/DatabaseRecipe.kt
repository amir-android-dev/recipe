package com.ema.recipe.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@TypeConverters(TypeConverterRecipe::class)
@Database(entities = [RecipeEntity::class], version = 1, exportSchema = true)
abstract class DatabaseRecipe : RoomDatabase() {

    abstract fun dao(): DaoRecipe
}