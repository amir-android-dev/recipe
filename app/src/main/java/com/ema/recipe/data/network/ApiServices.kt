package com.ema.recipe.data.network

import com.ema.recipe.data.model.recipe.ResponseRecipe
import com.ema.recipe.data.model.register.BodyRegister
import com.ema.recipe.data.model.register.ResponseRegister
import com.ema.recipe.util.Constants.API_KEY
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface ApiServices {

    @POST("/users/connect")
    suspend fun postRegister(
        @Query(API_KEY) apiKey: String,
        @Body body: BodyRegister
    ): Response<ResponseRegister>

    @GET("/recipes/complexSearch")
    suspend fun getRecipes(
        @QueryMap queries: Map<String, String>
    ): Response<ResponseRecipe>


}