package com.ema.recipe.data.repository

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import com.ema.recipe.data.model.menu.StoreMenuDataModel
import com.ema.recipe.util.Constants
import com.ema.recipe.util.Constants.MENU_MAIN_KEY
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException
import javax.inject.Inject
import javax.inject.Named

@ActivityRetainedScoped
class MenuRepository @Inject constructor(@Named(MENU_MAIN_KEY) private val dataStore: DataStore<Preferences>) {

    private object storeKey {
        val selectedMealId = intPreferencesKey(Constants.MENU_MEAL_ID)
        val selectedMealItem = stringPreferencesKey(Constants.MENU_MEAL_ITEM)
        val selectedDietId = intPreferencesKey(Constants.MENU_DIET_ID)
        val selectedDietItem = stringPreferencesKey(Constants.MENU_DIET_ITEM)
    }

    suspend fun saveMenuData(meal: String, mealId: Int, diet: String, dietID: Int) {
        dataStore.edit {
            it[storeKey.selectedMealItem] = meal
            it[storeKey.selectedMealId] = mealId
            it[storeKey.selectedDietItem] = diet
            it[storeKey.selectedDietId] = dietID
        }
    }

    val readMenuData: Flow<StoreMenuDataModel> = dataStore.data.catch { e ->
        if (e is IOException) {
            emit(emptyPreferences())
        } else {
            throw e
        }
    }.map {
        val selectMeal = it[storeKey.selectedMealItem] ?: Constants.MAIN_COURSE
        val selectMealId = it[storeKey.selectedMealId] ?: 0
        val selectDiet = it[storeKey.selectedDietItem] ?: Constants.DIET
        val selectDietId = it[storeKey.selectedDietId] ?: 0
        StoreMenuDataModel(selectMealId, selectMeal, selectDietId, selectDiet)
    }
}