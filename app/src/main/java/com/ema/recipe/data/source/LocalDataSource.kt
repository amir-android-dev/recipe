package com.ema.recipe.data.source


import com.ema.recipe.data.database.DaoRecipe
import com.ema.recipe.data.database.RecipeEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LocalDataSource @Inject constructor(private val daoRecipe: DaoRecipe) {


    suspend fun storeRecipe(recipeEntity: RecipeEntity) = daoRecipe.storeRecipe(recipeEntity)

    fun getAllRecipe(): Flow<List<RecipeEntity>> = daoRecipe.getAllRecipe()
}