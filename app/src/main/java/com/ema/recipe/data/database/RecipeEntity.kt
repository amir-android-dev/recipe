package com.ema.recipe.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ema.recipe.data.model.recipe.ResponseRecipe
import com.ema.recipe.util.Constants.RECIPE_TABLE

@Entity(tableName = RECIPE_TABLE)
data class RecipeEntity(
    @PrimaryKey(autoGenerate = false)
    var id: Int = 0,
    var response: ResponseRecipe
)
