package com.ema.recipe.data.source

import com.ema.recipe.data.model.register.BodyRegister
import com.ema.recipe.data.network.ApiServices
import retrofit2.http.QueryMap
import javax.inject.Inject

class RemoteDataSource @Inject constructor(private val api: ApiServices) {
    suspend fun postRegister(
        apiKey: String,
        body: BodyRegister
    ) = api.postRegister(apiKey, body)

    suspend fun getRecipes(
        @QueryMap queries: Map<String, String>
    ) = api.getRecipes(queries)
}