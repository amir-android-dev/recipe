package com.ema.recipe.data.repository

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringPreferencesKey
import com.ema.recipe.data.model.register.BodyRegister
import com.ema.recipe.data.model.register.StoreRegisterDataModel
import com.ema.recipe.data.source.RemoteDataSource
import com.ema.recipe.util.Constants
import com.ema.recipe.util.Constants.REGISTER_HASH
import com.ema.recipe.util.Constants.REGISTER_USER_INFO
import com.ema.recipe.util.Constants.REGISTER_USER_NAME
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException
import javax.inject.Inject
import javax.inject.Named

@ActivityRetainedScoped
class RegisterRepository @Inject constructor(
    @Named(REGISTER_USER_INFO) private val dataStore: DataStore<Preferences>,
    private val remote: RemoteDataSource
) {
    //store user info
    private object StoreKeys {
        val username = stringPreferencesKey(REGISTER_USER_NAME)
        val hash = stringPreferencesKey(REGISTER_HASH)
    }

    suspend fun storeRegisterData(username: String, hash: String) {
        dataStore.edit {
            it[StoreKeys.username] = username
            it[StoreKeys.hash] = hash
        }
    }

    val readRegisterData: Flow<StoreRegisterDataModel> = dataStore.data.catch { e ->
        if (e is IOException) {
            emit(emptyPreferences())
        } else {
            throw e
        }
    }.map {
        val username = it[StoreKeys.username] ?: ""
        val hash = it[StoreKeys.hash] ?: ""
        StoreRegisterDataModel(username, hash)
    }

    //api call
    suspend fun postRegister(
        apiKey: String,
        body: BodyRegister
    ) = remote.postRegister(apiKey, body)
}