package com.ema.recipe.data.database

import androidx.room.TypeConverter
import com.ema.recipe.data.model.recipe.ResponseRecipe
import com.google.gson.Gson

class TypeConverterRecipe {
    private val gson = Gson()

    @TypeConverter
    fun recipeToJson(recipe: ResponseRecipe): String {
        return gson.toJson(recipe)
    }

    @TypeConverter
    fun jsonToRecipe(recipe: String): ResponseRecipe {
        return gson.fromJson(recipe,ResponseRecipe::class.java)
    }
}