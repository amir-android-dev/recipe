package com.ema.recipe.data.model.register

data class StoreRegisterDataModel(
    val username: String,
    val hash: String
)