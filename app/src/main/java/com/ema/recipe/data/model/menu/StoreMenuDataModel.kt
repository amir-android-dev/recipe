package com.ema.recipe.data.model.menu

data class StoreMenuDataModel(
    val mealId: Int,
    val mealItem: String,
    val dietId: Int,
    val dietItem: String
)
