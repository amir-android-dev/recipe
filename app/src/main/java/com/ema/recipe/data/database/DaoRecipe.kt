package com.ema.recipe.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ema.recipe.util.Constants.RECIPE_TABLE
import kotlinx.coroutines.flow.Flow

@Dao
interface DaoRecipe {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun storeRecipe(recipeEntity: RecipeEntity)

    @Query("select * from $RECIPE_TABLE order by id ASC")
    fun getAllRecipe(): Flow<List<RecipeEntity>>
}