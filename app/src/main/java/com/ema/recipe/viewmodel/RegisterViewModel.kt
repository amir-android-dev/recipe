package com.ema.recipe.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ema.recipe.data.model.register.BodyRegister
import com.ema.recipe.data.model.register.ResponseRegister
import com.ema.recipe.data.repository.RegisterRepository
import com.ema.recipe.util.NetworkRequest
import com.ema.recipe.util.NetworkResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(private val repository: RegisterRepository) :
    ViewModel() {
    //api
    private val _registerData = MutableLiveData<NetworkRequest<ResponseRegister>>()
    val registerData: LiveData<NetworkRequest<ResponseRegister>>
        get() = _registerData

    fun callRegisterApi(apiKey: String, body: BodyRegister) = viewModelScope.launch {
        _registerData.value = NetworkRequest.Loading()
        val response = repository.postRegister(apiKey, body)
        _registerData.value = NetworkResponse(response).generalNetworkResponse()
    }

    //store response data
    fun storeData(username: String, hash: String) = viewModelScope.launch {
        repository.storeRegisterData(username, hash)
    }

    val readData = repository.readRegisterData
}