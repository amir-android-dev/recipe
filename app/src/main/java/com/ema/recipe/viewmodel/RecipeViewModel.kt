package com.ema.recipe.viewmodel

import androidx.lifecycle.*
import com.ema.recipe.data.database.RecipeEntity
import com.ema.recipe.data.model.recipe.ResponseRecipe
import com.ema.recipe.data.repository.MenuRepository
import com.ema.recipe.data.repository.RecipeRepository
import com.ema.recipe.util.Constants.ADD_RECIPEIN_FORMATION
import com.ema.recipe.util.Constants.API_KEY
import com.ema.recipe.util.Constants.DIET
import com.ema.recipe.util.Constants.GLUTEN_FREE
import com.ema.recipe.util.Constants.MAIN_COURSE
import com.ema.recipe.util.Constants.MY_API_KEY
import com.ema.recipe.util.Constants.NUMBER
import com.ema.recipe.util.Constants.POPULARITY
import com.ema.recipe.util.Constants.SORT
import com.ema.recipe.util.Constants.TRUE
import com.ema.recipe.util.Constants.TYPE
import com.ema.recipe.util.NetworkRequest
import com.ema.recipe.util.NetworkResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RecipeViewModel @Inject constructor(
    private val repository: RecipeRepository,
    private val menuRepository: MenuRepository
) : ViewModel() {
    /***popular***/
    private fun popularQueries(): HashMap<String, String> {
        val queries: HashMap<String, String> = HashMap()
        queries[API_KEY] = MY_API_KEY
        queries[SORT] = POPULARITY
        queries[NUMBER] = "10"
        queries[ADD_RECIPEIN_FORMATION] = TRUE
        return queries
    }

    //api
    private val _popularRecipes = MutableLiveData<NetworkRequest<ResponseRecipe>>()
    val popularRecipes: LiveData<NetworkRequest<ResponseRecipe>>
        get() = _popularRecipes

    fun callPopularApi() = viewModelScope.launch {
        _popularRecipes.value = NetworkRequest.Loading()
        val response = repository.remote.getRecipes(popularQueries())
        _popularRecipes.value = NetworkResponse(response).generalNetworkResponse()
        //store(cash) data
        val cashData = _popularRecipes.value?.data
        if (cashData != null) offlinePopular(cashData)
    }

    //local
    private fun storePopular(entity: RecipeEntity) = viewModelScope.launch(Dispatchers.IO) {
        repository.local.storeRecipe(entity)
    }


    val readPopularFromDb: LiveData<List<RecipeEntity>> =
        repository.local.getAllRecipe().asLiveData()

    private fun offlinePopular(response: ResponseRecipe) {
        val entity = RecipeEntity(0, response)
        storePopular(entity)
    }

    /***recent***/
    private var mealType = MAIN_COURSE
    private var dietType = GLUTEN_FREE
    private fun recentQueries(): HashMap<String, String> {
        viewModelScope.launch {
            menuRepository.readMenuData.collect {
                mealType = it.mealItem
                dietType = it.dietItem
            }
        }
        val queries: HashMap<String, String> = HashMap()
        queries[API_KEY] = MY_API_KEY
        queries[TYPE] = MAIN_COURSE
        queries[DIET] = GLUTEN_FREE
        queries[NUMBER] = "50"
        queries[ADD_RECIPEIN_FORMATION] = TRUE
        return queries
    }

    private val _recentRecipes = MutableLiveData<NetworkRequest<ResponseRecipe>>()
    val recentRecipes: LiveData<NetworkRequest<ResponseRecipe>>
        get() = _recentRecipes

    fun callRecentApi() = viewModelScope.launch {
        _recentRecipes.value = NetworkRequest.Loading()
        val response = repository.remote.getRecipes(recentQueries())
        _recentRecipes.value = NetworkResponse(response).generalNetworkResponse()
        //data cashing
        val cashData = _recentRecipes.value?.data
        if (cashData != null) offlineRecent(cashData)
    }

    //local
    val readRecentFromDb: LiveData<List<RecipeEntity>> =
        repository.local.getAllRecipe().asLiveData()

    private fun storeRecent(entity: RecipeEntity) = viewModelScope.launch(Dispatchers.IO) {
        repository.local.storeRecipe(entity)
    }

    private fun offlineRecent(response: ResponseRecipe) {
        val entity = RecipeEntity(1, response)
        storeRecent(entity)
    }

}
/*
  private fun recentNetworkResponse(response:Response<ResponseRecipe>):NetworkRequest<ResponseRecipe>{
        return when {
            response.message().contains("timeout") -> NetworkRequest.Error("Timeout")
            response.code() == 401 -> NetworkRequest.Error("You are not authorized")
            response.code() == 402 -> NetworkRequest.Error("Your free access is expired")
            response.code() == 422 -> NetworkRequest.Error("API KEY not found")
            response.code() == 500 -> NetworkRequest.Error("Pls try later!")
            response.body()!!.results.isNullOrEmpty()-> NetworkRequest.Error("Recipes not found")
            response.isSuccessful -> NetworkRequest.Success(response.body()!!)
            else -> NetworkRequest.Error(response.message())

        }
    }
 */