package com.ema.recipe.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ema.recipe.data.repository.MenuRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MenuViewModel @Inject constructor(private val repo:MenuRepository) : ViewModel() {

    fun mealsList(): MutableList<String> {
        return mutableListOf(
            "main course", "side dish", "dessert",
            "appetizer", "salad", "bread", "breakfast",
            "soup", "beverage", "sauce", "marinade",
            "fingerfood", "snack", "drink"
        )
    }

    fun dietList(): MutableList<String> {
        return mutableListOf(
            "Gluten Free", "Ketogenic", "Vegetarian",
            "Lacto-Vegetarian", "Ovo-Vegetarian", "Vegan",
            "Pescetarian", "Paleo", "Primal", "Low FODMAP",
            "Whole30"
        )
    }

    fun storeMenuItem(meal: String, mealId: Int, diet: String, dietID: Int) = viewModelScope.launch {
        repo.saveMenuData(meal, mealId, diet, dietID)
    }

    val readStoredMenuItems = repo.readMenuData
}